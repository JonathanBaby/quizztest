package com.example.fabien.quizztest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import dto.ResponseDto;

/**
 * Created by jo on 23/01/15.
 */
public class AnswersActivity extends ActionBarActivity {

    static final String key = "AnswersActivity.key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answers);

        Intent intent = getIntent();
        String responseType = intent.getStringExtra(AnswersActivity.key);

        final TextView answerView = (TextView) findViewById(R.id.textView);
        answerView.setText(responseType);

        Button buttonNextQuestion = (Button) findViewById(R.id.buttonNextQuestion);
        buttonNextQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }

        });
    }
}
