package com.example.fabien.quizztest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import dto.QuestionListDto;
import dto.ResponseDto;
import parsers.json.JsonQuestion;
import dto.QuestionDto;

/**
 * Created by fabien on 29/12/14.
 */
public class QuestionsActivity extends ActionBarActivity {

    private static final int ANSWER_ACTIVITY_REQUEST_CODE = 28;
    public static final String TAG = AnswersActivity.class.getSimpleName();
    private RadioGroup mRadioAnswerGroup;
    private RadioButton mRadioAnswer;
    private int mCurrentQuestionIndex = 0;
    private QuestionListDto mQuestionDtoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

        mQuestionDtoList = new QuestionListDto(getFromAssetsTextFile("questions.json"));


        initQuestion(mQuestionDtoList.getQuestionDtoList().get(mCurrentQuestionIndex));
    }



    private void initQuestion(final QuestionDto questionDto) {
        createQuestionInterface(questionDto);

        Button validateButton = (Button) findViewById(R.id.validatebutton);
        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = mRadioAnswerGroup.getCheckedRadioButtonId();
                mRadioAnswer = (RadioButton) findViewById(selectedId);
                View radioButton = mRadioAnswerGroup.findViewById(selectedId);
                int responseIndex = mRadioAnswerGroup.indexOfChild(radioButton);

                if (responseIndex < questionDto.getResponseDtos().size() && responseIndex >= 0) {
                    ResponseDto selectedReponse = questionDto.getResponseDtos().get(responseIndex);

                    if (mRadioAnswer != null) {
                        Intent monIntent = new Intent(QuestionsActivity.this, AnswersActivity.class);
                        monIntent.putExtra(AnswersActivity.key, selectedReponse.getType());
                        startActivityForResult(monIntent, ANSWER_ACTIVITY_REQUEST_CODE);
                    }

                } else {
                    Toast.makeText(QuestionsActivity.this, "Choisissez une réponse.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, " requestCode = "+ requestCode + " resultCode = "+ resultCode);
        if( requestCode == ANSWER_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK)
        {
            mCurrentQuestionIndex++;
            Toast.makeText(this, TAG +" currentIndex = "+ Integer.toString(mCurrentQuestionIndex), Toast.LENGTH_LONG).show();
            initQuestion(mQuestionDtoList.getQuestionDtoList().get(mCurrentQuestionIndex));
        }
    }

    private void createQuestionInterface(QuestionDto question) {

        final TextView questionView = (TextView) findViewById(R.id.question);
        questionView.setText(question.getText());
        mRadioAnswerGroup = (RadioGroup) findViewById(R.id.radioAnswerGroup);
        mRadioAnswerGroup.removeAllViews();
        for (ResponseDto value : question.getResponseDtos()) {
            RadioButton answer = new RadioButton(this);
            answer.setText(value.getText());
            mRadioAnswerGroup.addView(answer);
        }
    }


    public String getFromAssetsTextFile(String filename) {
        String tContents = "";
        try {
            InputStream stream = getAssets().open(filename);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }
        return tContents;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this, "OOOOOOOOOOOOHHHHHH !!!", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class CreationQuestionView {

    }

    /* @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }/**/




}
