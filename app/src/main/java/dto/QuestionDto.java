package dto;

import java.util.List;

public class QuestionDto {
    String text;

    public List<ResponseDto> getResponseDtos() {
        return responseDtos;
    }

    List<ResponseDto> responseDtos;
    int correctResponseId;

    public QuestionDto(String question, int correctResponseId, List<ResponseDto> responseDtos) {
        this.text = question;
        this.correctResponseId = correctResponseId;
        this.responseDtos = responseDtos;
    }

    public String getText() {
        return text;
    }

    public int getCorrectResponseId() {
        return correctResponseId;
    }
}