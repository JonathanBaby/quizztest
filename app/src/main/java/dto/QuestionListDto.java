package dto;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import parsers.json.JsonQuestion;
import parsers.json.JsonQuestionList;

/**
 * Created by jo on 26/01/15.
 */
public class QuestionListDto {
    List<QuestionDto> mQuestionDtoList;

    public QuestionListDto(String jsonContent) {
        mQuestionDtoList = parseQuestionListFromJSON(jsonContent);
    }

    public List<QuestionDto> getQuestionDtoList() {
        return mQuestionDtoList;
    }

    private List<QuestionDto> parseQuestionListFromJSON(String jsonContent) {
        List<QuestionDto> questionDtoList = new ArrayList<>();
        Gson gson = new Gson();
        JsonQuestion[] jsonQuestions = gson.fromJson(jsonContent, JsonQuestionList.class).getQuestions();
        for (JsonQuestion jsonQuestion : jsonQuestions) {
            questionDtoList.add(new QuestionDto(jsonQuestion.getText(), jsonQuestion.getCorrectResponseId(), jsonQuestion.getResponses()));
        }
        return questionDtoList;
    }
}
