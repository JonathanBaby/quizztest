package dto;

public class ResponseDto {
    String text;

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    String type;

    public ResponseDto(String text, String type) {
        this.text = text;
        this.type = type;
    }
}