package parsers.json;

import java.util.ArrayList;
import java.util.List;

import dto.ResponseDto;

public class JsonQuestion {
    String text;
    JsonResponse[] responses;

    public List<ResponseDto> getResponses() {
        List<ResponseDto> responseDtos = new ArrayList<>(responses.length);
        for (JsonResponse response : responses) {
            responseDtos.add(new ResponseDto(response.text, response.type));
        }
        return responseDtos;
    }

    public String getText() {
        return text;
    }

    public int getCorrectResponseId() {
        int index = 0;
        for (JsonResponse response : responses) {
            if (response.type.equals("valid"))
                return index;
            index++;
        }
        return index;
    }
}
